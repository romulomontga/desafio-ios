//
//  PullRequest.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 28/09/2017.
//

import Foundation

class PullRequest: Codable {
    
    var title: String
    var body: String?
    var user: User
    var date: String
    var url: URL
    
    fileprivate enum CodingKeys: String, CodingKey {
        case title
        case body
        case user
        case date = "created_at"
        case url = "html_url"
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        body = try values.decodeIfPresent(String.self, forKey: .body)
        user = try values.decode(User.self, forKey: .user)
        date = try values.decode(String.self, forKey: .date)
        url = try values.decode(URL.self, forKey: .url)
        
        if body == "" {
            body = "<No Description>"
        }
    }
    
    
    
}
