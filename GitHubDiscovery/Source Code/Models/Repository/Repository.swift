//
//  Repositorys.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 27/09/2017.
//

import Foundation

class Repository: Codable {
    
    var repositoryName: String
    var owner: User
    var description: String?
    var forks: Int
    var stars: Int
    
    fileprivate enum CodingKeys: String, CodingKey {
        case repositoryName = "name"
        case owner
        case description
        case forks
        case stars = "stargazers_count"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        repositoryName = try values.decode(String.self, forKey: .repositoryName)
        owner = try values.decode(User.self, forKey: .owner)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        forks = try values.decode(Int.self, forKey: .forks)
        stars = try values.decode(Int.self, forKey: .stars)
        
        
        if description == "" {
            description = "<No Description>"
        }
        
    }
    
    
}
