//
//  RepositorysContainer.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 27/09/2017.
//

import Foundation

class RepositoryContainer: Codable {
    
    var repositories: [Repository] = []
    
    fileprivate enum CodingKeys: String, CodingKey {
        case repositories = "items"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        repositories = try values.decode([Repository].self, forKey: .repositories)
    }
    
    
    
}
