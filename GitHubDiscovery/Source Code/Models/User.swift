//
//  User.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 27/09/2017.
//

import Foundation

class User: Codable {
    
    var id: Int
    var name: String
    var avatar: URL
    
    fileprivate enum CodingKeys: String, CodingKey {
        case id
        case name = "login"
        case avatar = "avatar_url"
    }
    
    required init(from decoder: Decoder) throws {
        
        name = "ghost"
        avatar = URL(string: "https://avatars1.githubusercontent.com/u/10137?v=4&s=88")!
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        avatar = try values.decode(URL.self, forKey: .avatar)
    }
    
    
    
}
