//
//  GitHubAPI.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 27/09/2017.
//

import Foundation
import Alamofire


class ServiceHelper {
    
    fileprivate let baseUrl = "https://api.github.com/"
    
    enum Result<T> {
        case success(T)
        case error(Error)
    }
    
    func loadRepository(page: Int, language: String, completion: @escaping (Result<RepositoryContainer>) -> Void) {
        
        guard let url = URL(string: "\(baseUrl)search/repositories?q=language:\(language)&sort=stars&page=\(page)") else {
            return
        }
        
        Alamofire.request(url).responseData { response in
            switch response.result {
            case .success(let data):
                do {
                    let jsonDecoder = JSONDecoder()
                    let repositoryContainer = try jsonDecoder.decode(RepositoryContainer.self, from: data)
                    completion(.success(repositoryContainer))
                } catch {
                    completion(.error(error))
                }
                
            case .failure(let error):
                completion(.error(error))
            }
        }
    }
    
    func loadPullRequests(creator: String, repositoryName: String, completion: @escaping (Result<[PullRequest]>) -> Void) {
        
        guard let url = URL(string: "\(baseUrl)repos/\(creator)/\(repositoryName)/pulls") else {
            return
        }
        
        Alamofire.request(url).responseData { response in
            
            switch response.result {
            case .success(let data):
                do {
                    let jsonDecoder = JSONDecoder()
                    let pullRequestResponse = try jsonDecoder.decode([PullRequest].self, from: data)
                    completion(.success(pullRequestResponse))
                } catch {
                    completion(.error(error))
                }
                
            case .failure(let error):
                completion(.error(error))
            }
        }
        
    }
    
}
