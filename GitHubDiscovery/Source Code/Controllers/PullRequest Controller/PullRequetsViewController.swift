//
//  PullRequetsViewController.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 28/09/2017.
//

import UIKit

class PullRequetsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet private weak var mainTableView: UITableView!
    
    public var repositoryInfo: Repository?
    private var service = ServiceHelper()
    private var dataSource: [PullRequest]? = []
    
    // MARK - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuringTableView()
        loadPullRequestData()
    }
    
    // MARK - Configuring TableView
    private func configuringTableView() {
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.estimatedRowHeight = RepositoryTableViewCell.cellHeight()
        mainTableView.rowHeight = UITableViewAutomaticDimension
        mainTableView.indicatorStyle = UIScrollViewIndicatorStyle.white

        mainTableView.register(UINib(nibName: "PullRequestTableViewCell", bundle: nil), forCellReuseIdentifier: "pullRequestCell")
    }
    
    // MARK - Loading Data In To TableView
    private func loadPullRequestData() {
        
        showProgressLoading()
        
        if let ownerName = repositoryInfo?.owner.name, let repoName = repositoryInfo?.repositoryName {
            service.loadPullRequests(creator: ownerName, repositoryName: repoName) { [weak self] result in
                switch result {
                case .success(let data):
                    self?.dismissProgressLoading()
                    self?.dataSource = data
                    DispatchQueue.main.async {
                        self?.mainTableView.reloadData()
                    }
                    
                case .error(let error):
                    self?.dismissProgressLoading()
                    print(error)
                }
            }
        }
    }
    
    // MARK: - UITableView Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataSource = dataSource {
            return dataSource.count
        } else {
            return 0
        }
    }
    
    // MARK: - UITableView Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositoryTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullRequestCell", for: indexPath) as! PullRequestTableViewCell
        
        cell.pullRequest = dataSource?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if let dataSource = dataSource {
            let pullRequest = dataSource[indexPath.row]
            UIApplication.shared.openURL(pullRequest.url)
        }
    }
    
}


