//
//  PullRequestTableViewCell.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 29/09/2017.
//

import UIKit
import SDWebImage

class PullRequestTableViewCell: UITableViewCell {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var bodyTextView: UITextView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    override func prepareForReuse() {
        nameLabel.text = ""
        avatarImageView.image = UIImage(named: "icon-placeholder-user")
        bodyTextView.text = ""
        titleLabel.text = ""
        dateLabel.text = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.clipsToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.sd_setShowActivityIndicatorView(true)
        avatarImageView.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
    }
    
    var pullRequest: PullRequest? {
        didSet {
            self.nameLabel.text = pullRequest?.user.name
            self.titleLabel.text = pullRequest?.title
            self.bodyTextView.text = pullRequest?.body
            self.avatarImageView.sd_setImage(with: pullRequest?.user.avatar)
            
            if let date = pullRequest?.date {
                self.dateLabel.text = convertDateFormater(date: date)
            }
        }
    }
    
    func convertDateFormater(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: date)!
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    class func cellHeight() -> CGFloat {
        return 136
    }
    
}
