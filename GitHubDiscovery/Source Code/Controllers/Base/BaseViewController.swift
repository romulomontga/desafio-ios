//
//  BaseViewController.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 26/09/17.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController {
    
    // MARK: - Loading HUD Methods
    internal func showProgressLoading() {
        SVProgressHUD .show()
    }
    
    internal func dismissProgressLoading() {
        SVProgressHUD .dismiss()
    }
    
}
