//
//  BaseNavegationViewController.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 28/09/2017.
//

import UIKit

class BaseNavegationViewController: UINavigationController {
    
    // MARK: - Status Bar Style
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
}
