//
//  RepositoryTableViewCell.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 27/09/2017.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var ownerAvatarImageView: UIImageView!
    @IBOutlet private weak var ownerName: UILabel!
    @IBOutlet private weak var repositoryName: UILabel!
    @IBOutlet private weak var repositoryDescription: UITextView!
    @IBOutlet private weak var numbersOfForks: UILabel!
    @IBOutlet private weak var numbersOfStar: UILabel!
    
    override func prepareForReuse() {
        repositoryName.text = ""
        repositoryDescription.text = ""
        numbersOfForks.text = "0"
        numbersOfStar.text = "0"
        ownerAvatarImageView.image = UIImage(named: "icon-placeholder-user")
        ownerName.text = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ownerAvatarImageView.clipsToBounds = true
        ownerAvatarImageView.layer.cornerRadius = ownerAvatarImageView.frame.size.width / 2
        ownerAvatarImageView.sd_setShowActivityIndicatorView(true)
        ownerAvatarImageView.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
    }
    
    var repository: Repository? {
        didSet {
            
            self.ownerAvatarImageView.sd_setImage(with: repository?.owner.avatar)
            self.repositoryName.text = repository?.repositoryName
            self.repositoryDescription.text = repository?.description
            self.ownerName.text = repository?.owner.name
            self.numbersOfStar.text = "\(repository?.stars ?? 0)"
            self.numbersOfForks.text = "\(repository?.forks ?? 0)"
        }
    }
    
    class func cellHeight() -> CGFloat {
        return 136
    }
    
}
