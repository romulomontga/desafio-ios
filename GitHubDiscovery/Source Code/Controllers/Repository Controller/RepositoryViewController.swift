//
//  ViewController.swift
//  GitHubDiscovery
//
//  Created by Romulo Monteiro on 25/09/17.
//

import UIKit
import Alamofire

class RepositoryViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK - IBOutlets
    @IBOutlet private weak var mainTableView: UITableView!
    
    private var service = ServiceHelper()
    private var dataSource: [Repository]? = []
    private let language: String = "java"       //Setting a language to search in github
    private var pageNumber: Int = 1     //Initial page number
    
    // MARK - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuringTableView()
        loadGitData(numberOfPage: pageNumber)
    }
    
    // MARK - Configuring TableView
    private func configuringTableView() {
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.estimatedRowHeight = RepositoryTableViewCell.cellHeight()
        mainTableView.rowHeight = UITableViewAutomaticDimension
        mainTableView.indicatorStyle = UIScrollViewIndicatorStyle.white
        
        mainTableView.register(UINib(nibName: "RepositoryTableViewCell", bundle: nil), forCellReuseIdentifier: "RepositoryCell")
    }
    
    // MARK - Loading Data In To TableView
    private func loadGitData(numberOfPage: Int) {
        
        if (numberOfPage == 1) {
            showProgressLoading()
        }
        
        service.loadRepository(page: numberOfPage, language: language) { [weak self] result in
            switch result {
            case .success(let data):
                if (numberOfPage != 1){
                    self?.dataSource?.append(contentsOf: data.repositories)
                } else {
                    self?.dismissProgressLoading()
                    self?.dataSource = data.repositories
                }
                DispatchQueue.main.async {
                    self?.mainTableView.reloadData()
                }
                
            case .error(let error):
                self?.dismissProgressLoading()
                print(error)
            }
        }
        
    }
    // MARK: - UITableView Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataSource = dataSource {
            return dataSource.count
        } else {
            return 0
        }
    }
    
    // MARK: - UITableView Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositoryTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let dataSource = dataSource {
            if dataSource.count - 10 == indexPath.row {
                pageNumber += 1
                loadGitData(numberOfPage: pageNumber)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryTableViewCell
        
        cell.repository = dataSource?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "toPullRequestSegue", sender: dataSource?[indexPath.row])
        
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPullRequestSegue"{
            let destination: PullRequetsViewController? = (segue.destination as? PullRequetsViewController)
            destination?.repositoryInfo = sender as? Repository
        }
    }
}
